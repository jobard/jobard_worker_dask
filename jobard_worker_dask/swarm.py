"""Jobard worker dask module."""
import re
import socket
import time

import click
from distributed import Worker


def dask_worker_hostname(mapping_tcp_host: str, mapping_tcp_port: str, max_loop:  int = 20) -> str:
    """Retrieve dask worker contact point (real host name and port).

    Args:
        mapping_tcp_host: dask scheduler host
        mapping_tcp_port: dask scheduler port
        max_loop: nb tries ; default = 20

    Returns:
        Dask  hostname

    Raises:
        ConnectionError: if dask worker failed to start too many tries
    """
    cpt: int = 0
    while cpt < max_loop:
        client = None
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((mapping_tcp_host, int(mapping_tcp_port)))
            client.send(socket.gethostname().encode('ascii'))
            client_input = client.recv(1024).decode('ascii').strip()
            print('reading from socket : {0}'.format(client_input))
            if '|' in client_input:
                return client_input
            time.sleep(10)
        except Exception as error:
            print('non-fatal failed')
            print(error)
        finally:
            if client is not None:
                client.close()
        cpt += 1

    raise ConnectionError(
        'too many failed tries to get the worker mapping contact-address from the jobard remote dask',
    )


@click.command()
@click.option('--mapping-tcp-host', required=True, type=str)
@click.option('--mapping-tcp-port', required=True, type=str)
def dask_setup(worker: Worker, mapping_tcp_host: str, mapping_tcp_port: str) -> None:
    """Redirect dask-worker's contact-address to swarm host directly.

    Args:
        worker: dask worker instance
        mapping_tcp_host: dask scheduler host
        mapping_tcp_port: dask scheduler port
    """
    # retrieve real worker host and port
    client_input = dask_worker_hostname(mapping_tcp_host, mapping_tcp_port)
    input_array = client_input.split('|')
    dask_worker_port_regex = re.search(r':(\d+)->9000', input_array[1], flags=re.IGNORECASE)

    # override dask worker contact point
    worker.contact_address = 'tcp://{0}:{1}'.format(input_array[0], dask_worker_port_regex.group(1))
    worker.name = socket.gethostname()
    print('overriding contact-address option : {0} '.format(worker.contact_address))
