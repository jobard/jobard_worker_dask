# jobard_worker_dask

Jobard dask worker initialization, used by `jobard_remote_dask` component.

## Installation

### docker image

```commandline
docker pull gitlab-registry.ifremer.fr/jobard/jobard_worker_dask
```
